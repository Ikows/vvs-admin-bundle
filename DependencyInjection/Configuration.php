<?php

namespace Ikows\Bundle\VVSAdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('vvs_admin');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('user_class')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
