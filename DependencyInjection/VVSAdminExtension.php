<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ikows\Bundle\VVSAdminBundle\DependencyInjection;

use App\Entity\User;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\Security\Core\User\UserInterface;

class VVSAdminExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        // Mandatory configuration.
        if (!isset($config['user_class'])) {
            throw new LogicException('The mandatory configuration property "user_class" in config/package/vvs_admin.yaml is missing or empty. Please specify it with the fully qualified user class name you want VVSAdmin to manage.');
        }
        if (!class_exists($config['user_class']) || !new $config['user_class'] instanceof UserInterface) {
            throw new LogicException('The user class : "' . $config['user_class']  . '" specified in property "user_class" of config/package/vvs_admin.yaml does not exists or does not implements ' . UserInterface::class );
        }

        $loader = new PhpFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('admin.php');


        $container->setParameter('vvs_admin.controller.user_class', $config['user_class']);

    }

}
