<?php

namespace Ikows\Bundle\VVSAdminBundle\Controller;

use App\Entity\User;
use Twig\Environment;
use Symfony\Component\Form\FormFactory;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class VVSAdminController
{
    private $templateManager;
    private $generator;
    private $twig;
    private $doctrine;
    private $serializer;
    private $templates;
    private $baseDir;
    private $userClass;

    public function __construct(
        UrlGeneratorInterface $generator,
        Environment $twig,
        Registry $doctrine,
        SerializerInterface $serializer,
        UserPasswordHasherInterface $hasher,
        FormFactory $formFactory,
        RequestStack $requestStack,
        array $templates,
        string $baseDir = null,
        string $userClass = null
        )
    {
        $this->generator = $generator;
        $this->twig = $twig;
        $this->doctrine = $doctrine;
        $this->serializer = $serializer;
        $this->hasher = $hasher;
        $this->formFactory = $formFactory;
        $this->requestStack = $requestStack;
        $this->templates = $templates;
        $this->baseDir = $baseDir;
        $this->userClass = $userClass;
    }

    public function dashboard(): Response
    {
        $users = $this->doctrine->getRepository($this->userClass)->findAll();

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
        ];
        $normalizer = new ObjectNormalizer($classMetadataFactory, null, null, null, null, null, $defaultContext);
        $serializer = new Serializer([$normalizer]);
        $normalizedUsers = $serializer->normalize($users);
        
        return new Response(
            $this->twig->render('@VVSAdmin/dashboard.html.twig', [
                'users' => $normalizedUsers
            ]),
            200,
            ['Content-Type' => 'text/html']
        );
    }


    public function editPassword(int $uid): Response
    {
        $request = $this->requestStack->getMainRequest();
        $user = $this->doctrine->getRepository($this->userClass)->find($uid);
        $formBuilder = $this->formFactory->createBuilder(FormType::class, $user);
        $form = $formBuilder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les champs mot de passe doivent correspondrent',
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        'max' => 4096,
                    ]),
                ],
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Reapeat password'],
                ])
            ->setAction('/vvs-admin/edit-password/' . $uid)
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($this->hasher->hashPassword($user, $form->get('plainPassword')->getData()));
            $entityManager = $this->doctrine->getManager();
            $entityManager->flush();

            $request->getSession()->getFlashBag()->add('success', 'ta grande tante');

            return new JsonResponse('ok boi', Response::HTTP_ACCEPTED);
        }
        
        return new Response(
            $this->twig->render('@VVSAdmin/edit_password.html.twig', [
                'form' => $form->createView()
            ]),
            200,
            ['Content-Type' => 'text/html']
        );
    }

}