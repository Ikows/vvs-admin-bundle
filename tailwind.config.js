/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ["./Resources/views/**/*.{html,js,twig}", "./public/js/**/*.js", "./assets/scss/**/*.scss"],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
