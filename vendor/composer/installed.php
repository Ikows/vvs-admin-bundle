<?php return array(
    'root' => array(
        'name' => 'ikows/vvs-admin-bundle',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'f3139a50d9b46a296d639c5eeca47ca29267a4ba',
        'type' => 'symfony-bundle',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'ikows/vvs-admin-bundle' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'f3139a50d9b46a296d639c5eeca47ca29267a4ba',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
