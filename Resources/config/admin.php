<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use Ikows\Bundle\VVSAdminBundle\Controller\VVSAdminController;

return static function (ContainerConfigurator $container) {
    $container->services()

        ->set('vvs_admin.controller.admin', VVSAdminController::class)
        ->public()
        ->args([
            service('router')->nullOnInvalid(),
            service('twig'),
            service('doctrine'),
            service('serializer'),
            service('security.user_password_hasher'),
            service('form.factory'),
            service('request_stack'),
            param('data_collector.templates'),
            param('kernel.project_dir'),
            param('vvs_admin.controller.user_class'),
        ])
    ;
};
