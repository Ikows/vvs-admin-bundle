const popoverHandler = {
    
    init: () => {
        const triggers = document.querySelectorAll('button[data-popover-target]')
        for (const trigger of triggers) {
            trigger.addEventListener('click', () => {
                setTimeout(() => {
                    let popover = document.querySelector(`div[data-popover="${trigger.dataset.popoverTarget}"]`)
                    popover.classList.remove('invisible', 'opacity-0')
                    popover.setAttribute('data-popover-visibility', 'visible')
                }, 10)
            })
        }

        document.addEventListener('click', (e) => {
            if (!e.target.classList.contains('popover-overlay')) {
                const popover = document.querySelector(`div[data-popover-visibility="visible"]`)
                if (popover) {
                    popover.classList.add('invisible', 'opacity-0')
                    popover.setAttribute('data-popover-visibility', 'invisible')
                }
            }
        })
    },
}
export { popoverHandler }