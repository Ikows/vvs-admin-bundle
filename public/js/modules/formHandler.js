const formHandler = {
    
    getForm: (action) => new Promise(function(resolve, reject) {
        let req = new XMLHttpRequest();
        req.open('GET', action);
        req.onload = function() {
            if (req.status == 200) {
                resolve(req.response);
            } else {
                reject("Error");
            }
        };
        req.send();
    }),

    submitForm: (action, formData) => new Promise(function(resolve, reject) {
        let req = new XMLHttpRequest();
        req.open('POST', action);
        req.onload = function() {
            if (req.status === 200 || req.status === 202) {
                resolve(req);
            } else {
                reject("Error");
            }
        };
        req.send(formData);
    })

}
export { formHandler }