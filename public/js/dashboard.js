import { formHandler } from "./modules/formHandler.js"
import { popoverHandler } from "./modules/popoverHandler.js";

function startTime() {
    const today = new Date();
    let h = today.getHours();
    let m = today.getMinutes();
    let s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    console.log(document.getElementById('dynamic-hour'));
    document.getElementById('dynamic-hour').innerHTML =  h + ":" + m + ":" + s;
    setTimeout(startTime, 1000);
}

function checkTime(i) {
    if (i < 10) {i = "0" + i};
    return i;
}

// Mobile menu btn
const mobileMenuBtn = document.getElementById('mobile-menu-btn')
const mobileMenu = document.getElementById('mobile-menu')
mobileMenuBtn.addEventListener('click', () => {
    mobileMenuBtn.setAttribute('aria-expanded', (mobileMenuBtn.getAttribute('aria-expanded') === "false"));
    mobileMenuBtn.querySelectorAll('svg').forEach((elem) => {
        elem.classList.toggle('block')
        elem.classList.toggle('hidden')
    })
    if (mobileMenuBtn.getAttribute('aria-expanded') === "true") {
        mobileMenu.classList.remove('hidden')
        mobileMenu.classList.remove('hidden-slide')
    } else {
        mobileMenu.classList.add('hidden-slide')
        setTimeout(() => mobileMenu.classList.add('hidden'), 500)
    }
})

// Popovers
popoverHandler.init()

// Modal.
const modal = document.getElementById("table-modal");
const modalCloseBtn = document.getElementById("table-modal-close");
modalCloseBtn.onclick = () => modal.style.display = "none"
window.onclick = (event) => {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// Edit Password forms.
for (const btn of document.querySelectorAll(".edit-password")) {
    btn.onclick = async function() {
        try {
            const id = this.parentElement.dataset.row;
            const resp = await formHandler.getForm(`/vvs-admin/edit-password/${id}`)
            document.querySelector('.modal-body').innerHTML = resp
            handleSubmit(document.querySelector('.modal-body form'))
            modal.style.display = "block"
        } catch (error) {
            console.log(error);
        }
    }
}

const handleSubmit = (form) => {
    form.onsubmit = async (e) => {
        e.preventDefault()
        const formData = new FormData(e.target)
        const req = await formHandler.submitForm(e.target.getAttribute('action'), formData)
        if (req.status === 200) {
            document.querySelector('.modal-body').innerHTML = req.response
            handleSubmit(document.querySelector('.modal-body form'))
        } else {
            modal.style.display = "none";
        }
    }
}